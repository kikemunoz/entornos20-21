package ejercicio05previo;

public class Ejercicio05Previo {

	public static void main(String[] args) {
		// suma de dos variables enteras
		int numero1 = 3;
		int numero2 = 5;
		int suma = numero1 + numero2;
		System.out.println("La suma es " + suma);
		// resta de dos variables enteras
		int resta = numero1 - numero2;
		System.out.println("La resta es " + resta);
		// multiplicacion de dos variables enteras
		int multiplica = numero1 * numero2;
		System.out.println("La multiplicacion es " + multiplica);
		// division de dos variables enteras
		int division = numero1 / numero2;
		System.out.println("La division entera es " + division);
		// division de dos variables doubles ??? -> no
		double division1 = numero1 / numero2;
		System.out.println("La division entera es " + division1);
		// division de dos variables doubles
		double numero3 = 3;
		double numero4 = 5;
		double division2 = numero3 / numero4;
		System.out.println("La division double es " + division2);

	}

}
