package ejercicio09plus;

public class Ejercicio09Plus {

	public static void main(String[] args) {
		System.out.println("Asignar un double casteando a entero");
		double numero1 = 3.25;
		int numero2;
		System.out.println("numero1 " + numero1);
		numero2 = (int) numero1;
		System.out.println("numero2 " + numero2);
		
		System.out.println("Asignar un entero a un double");
		int numero3 = 10;
		double numero4;
		numero4 = numero3;
		System.out.println("numero3 " + numero3);
		System.out.println("numero4 " + numero4);
		int numero5 = 3;
		int numero6 = 5;
		double division = (double) numero5 / (double) numero6;
		System.out.println("numero5 " + numero5);
		System.out.println("numero6 " + numero6);
		System.out.println("division " + division);
		// double division = (double) (numero5 / numero6);
		// esto no sera correcto
		// la division es entera y aunque se guarda en un double
		// no guarda los decimales, se han truncado
		char letra = 'A';
		System.out.println("letra " + letra);
		int numerito = (int) letra;
		System.out.println("numerito " + numerito);
		int numerito1 = 126;
		char letra1 = (char) numerito1;
		System.out.println("letra1 " + letra1);

	}

}
