package ejercicio05;

public class Ejercicio05 {

	public static void main(String[] args) {
		// resto de dos variables enteras
		int numero1 = 6;
		int numero2 = 4;
		int resto = numero1 % numero2;
		System.out.println("El resto es " + resto);
		// resto de dos variables enteras con resto cero
		int numero3 = 4;
		int numero4 = 2;
		int resto1 = numero3 % numero4;
		System.out.println("El resto es " + resto1);

	}

}
