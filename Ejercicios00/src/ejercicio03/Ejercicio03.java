package ejercicio03;

public class Ejercicio03 {

	public static void main(String[] args) {
		// Realizar un programa que visualice el valor de una variable
		// de tipo cadena en el mensaje siguiente: �El valor de la variable es: �
		String cadena="hola caracola";
		// primera forma
		System.out.print("El valor de la variable es: ");
		System.out.println(cadena);
		// segunda forma
		System.out.println("El valor de la variable es: "+cadena);

	}

}
