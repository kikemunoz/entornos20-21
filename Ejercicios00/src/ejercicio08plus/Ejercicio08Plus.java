package ejercicio08plus;

public class Ejercicio08Plus {

	public static void main(String[] args) {
		int num1 = 56;
		int num2 = -14;
		String resultado = num2 > num1 ? "num2 es mayor" : "num1 es mayor";
		System.out.println(resultado);
	}

}
