package ejercicio08;

public class Ejercicio08 {

	public static void main(String[] args) {
		// Declarar num1 y darle un valor entero
		int num1 = 25;
		// Declarar num2 y darle un valor entero
		int num2 = 5;
		// Declarar num3 y darle un valor entero
		int num3 = 4;
		// Declarar un booleano resultado
		boolean resultado;
		// Asignarle la comparacion num1>num3
		resultado = (num1 > num3);
		// Mostrar su resultado
		System.out.println(resultado);
		// A resultado, asignarle ahora la comparacion num3>num2
		resultado = (num3 > num2);
		// Mostrar su resultado
		System.out.println(resultado);
		// A resultado, asignarle la operacion AND de las dos anteriores
		resultado = (num1 > num3) && (num3 > num2);
		// Mostrar su resultado
		System.out.println(resultado);
		// A resultado, asignarle la operacion OR de las dos anteriores
		resultado = (num1 > num3) || (num3 > num2);
		// Mostrar su resultado
		System.out.println(resultado);
		// A resultado, asignarle true
		resultado = true;
		// Usar la operacion NOT para invertir su valor
		resultado = !resultado;
		// Mostrar su resultado
		System.out.println(resultado);

	}

}
