package ejercicio06;

public class Ejercicio06 {

	public static void main(String[] args) {
		double euros = 25;
		double cambioLibras = 0.91;
		double cambioDolares = 1.17;
		System.out.println("forma1");
		double cambioEurosLibras = euros * cambioLibras;
		double cambioEurosDolares = euros * cambioDolares;
		System.out.println("La cantidad en euros es " + euros);
		System.out.println("La cantidad en Libras es " + cambioEurosLibras);
		System.out.println("La cantidad en Dolares es " + cambioEurosDolares);
		System.out.println("forma2 menos automatica");
		System.out.println("La cantidad en euros es " + euros);
		System.out.println("La cantidad en Libras es " + euros * cambioLibras);
		System.out.println("La cantidad en Dolares es " + euros * cambioDolares);
		System.out.println("forma3 menos menos automatica");
		System.out.println("La cantidad en euros es " + euros);
		System.out.println("La cantidad en Libras es " + euros * 0.91);
		System.out.println("La cantidad en Dolares es " + euros * 1.17);

	}

}
